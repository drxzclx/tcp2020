import requests
import copy
import string
import random
import json

def newweapon(headers, data):
    weapon_template = {
      "_id": "*not a real id*",
      "name": "Chinese knockoff .22",
      "type": "weapon",
      "data": {
        "description": "",
        "quantity": 1,
        "weight": 0,
        "attributes": {
            "cost": {
            "type": "String",
            "label": "Cost",
            "value": "",
            "dtype": "String"
          },
          "source": {
            "type": "String",
            "label": "Source",
            "value": "",
            "dtype": "String"
          },
          "weapontype": {
            "type": "String",
            "label": "Weapon Type",
            "value": "P",
            "dtype": "String"
          },
          "wa": {
            "type": "Number",
            "label": "Weapon Accuracy",
            "value": "0",
            "dtype": "String"
          },
          "concealability": {
            "type": "String",
            "label": "Concealability",
            "value": "",
            "dtype": "String"
          },
          "availability": {
            "type": "String",
            "label": "Availability",
            "value": "",
            "dtype": "String"
          },
          "damage": {
            "type": "String",
            "label": "Damage",
            "value": "1d6",
            "dtype": "String"
          },
          "shots": {
            "type": "Number",
            "label": "Shots",
            "value": "1",
            "dtype": "String"
          },
          "rof": {
            "type": "Number",
            "label": "Rate of Fire",
            "value": "1",
            "dtype": "String"
          },
          "reliability": {
            "type": "String",
            "label": "Reliability",
            "value": "",
            "dtype": "String"
          },
          "range": {
            "type": "String",
            "label": "Range",
            "value": "",
            "dtype": "String"
          }
        }
      },
      "sort": 300001,
      "flags": {},
      "img": "icons/svg/mystery-man.svg"
    }

    def assign(weapon, key, value):
        if key == "Name":
            weapon["name"] = value
            print(value)
        elif key == "Type":
            # Sanitize weapon types
            value = value.split('(')[0].strip()
            
            wt_mapping = {
                "Melee":"MEL",
                "Heavy":"HVY",
                "Pistol":"P",
                "Rifle":"RIF",
                "Shotgun":"SHT",
                "Sub-machine gun":"SMG",
                "Rifle/SMG":"RIF",
                "Rifle/HVY":"RIF",
            }
            value = wt_mapping.get(value,value)
            if value == "Exotic":
                if "bow" in weapon["name"].lower():
                    # It's a bow
                    value = "BOW"
                else:
                    # It's another exotic, there's no uniform way to handle it.
                    pass
                            
            allowed_weapon_types="P,RIF,SMG,SHT,HVY,MEL,BOW,Exotic".split(',')
            if value not in allowed_weapon_types:
                raise ValueError("Unknown weapon type: %r" % value)
            weapon["data"]["attributes"]["weapontype"]["value"] = value
        elif key == "Accur.":
            weapon["data"]["attributes"]["wa"]["value"] = value
        elif key == "Conceal.":
            weapon["data"]["attributes"]["concealability"]["value"] = value
        elif key == "Avail.":
            weapon["data"]["attributes"]["availability"]["value"] = value
        elif key == "Damage/Ammo":
            # Todo make sure it's a clean roll
            # Todo find a place to store the ammunition type
            # Todo figure out what to do with the non-rollable damage types such as "Varies" or "Drugs"
            # Baseline, strip off everthing after "("
            roll = value.split('(')[0].strip()
            weapon["data"]["attributes"]["damage"]["value"] = roll
        elif key == "Range":
            weapon["data"]["attributes"]["range"]["value"] = value
        elif key == "Cost":
            weapon["data"]["attributes"]["cost"]["value"] = value
        elif key == "Book":
            weapon["data"]["attributes"]["source"]["value"] = value
        elif key == "#Shots":
            weapon["data"]["attributes"]["shots"]["value"] = value
        elif key == "RoF":
            weapon["data"]["attributes"]["rof"]["value"] = value
        elif key == "Reliab.":
            weapon["data"]["attributes"]["reliability"]["value"] = value
        else:
            raise ValueError("Unsupported key: %r" % key)

    #print(headers)
    weapon = copy.deepcopy(weapon_template)
    # Assign a random ID
    letters = string.ascii_lowercase
    weapon["_id"] = ''.join(random.choice(letters) for i in range(10))
    
    
    for k,v in zip(headers,data):
        assign(weapon, k, v);
    #print(weapon)
    return weapon

from bs4 import BeautifulSoup

r = requests.get("https://cyberpunk.fandom.com/wiki/Weapons_in_Cyberpunk_2020")
soup = BeautifulSoup(r.text)
tables = soup.find_all("table")

weapons = []
for table in tables:
    #print(table)
    headers = []
    for row in table.find_all("tr"):
        rowdata = []
        # check if we have headers!
        for th in row.find_all("th"):
            headers.append(th.text.strip())
        # process the tds
        for td in row.find_all("td"):
            rowdata.append(td.text.strip())
        if rowdata:
            if len(rowdata) == len(headers):
                try:
                    weapons.append(newweapon(headers, rowdata))
                except ValueError:
                    pass
        
with open("../packs/weapons.db","w") as pack:
    for weapon in weapons:
        print(json.dumps(weapon),file=pack)


