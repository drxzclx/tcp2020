/**
 * Extend the basic ActorSheet with some very simple modifications
 */
export class CP2020ActorSheet extends ActorSheet {
  constructor(...args) {
    super(...args);

    /**
     * Keep track of the currently active sheet tab
     * @type {string}
     */
    this._sheetTab = "stats";
	this._show_zero_skills = true;
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["tcp2020", "sheet", "actor"],
  	  template: "systems/tcp2020/templates/cp2020-actor-sheet.html",
      width: 600,
      height: 600
    });
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for rendering the Actor sheet
   * The prepared data object contains both the actor data as well as additional sheet options
   */
  getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];
    for ( let attr of Object.values(data.data.attributes) ) {
      attr.isCheckbox = attr.dtype === "Boolean";
    }
    return data;
  }

  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
activateListeners(html) {
    super.activateListeners(html);

    // Activate tabs
    let tabs = html.find('.tabs');
    let initial = this._sheetTab;
    new Tabs(tabs, {
      initial: initial,
      callback: clicked => this._sheetTab = clicked.data("tab")
    });
	
	// Toggle zero skill display
	html.find(".zeroskill-toggle")[0].addEventListener("click", ev => {this._onClickZeroSkillToggle(ev, this.actor);});


    // Rolls and uses are owner only!
    if ( this.actor.owner ) {
    
        // Skill rolls!
        let skills = html.find(".skill");
        skills.map(i => {
            skills[i].addEventListener("click", ev => {this._onClickSkill(ev, this.actor);});
        })	

        // Use item!
        let items = html.find(".owned-item");
        items.map(i => {
            items[i].addEventListener("click", ev => {this._onClickItem(ev, this.actor);});
        })	

        // Item actions!
        let itemactions = html.find('.item-action');
        itemactions.map( i => {
            itemactions[i].addEventListener("click", ev => {this._onItemAction(ev, this.actor);});
        });
    } else {
        // Remove rollable
        html.find(".rollable").each((i, el) => el.classList.remove("rollable"));
    }

    // Update visibility for zero skills
    this._updateSkillVisibility();

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Add or Remove Attribute
    html.find(".attributes").on("click", ".attribute-control", this._onClickAttributeControl.bind(this));		
  }
  
  
_updateSkillVisibility() {
	let show_zero_skills =  this._show_zero_skills;
	this.form.querySelectorAll('.skill-value').forEach(function(el) {
		if (show_zero_skills) {
			if (el.value == "0") {
				el.parentNode.style.display = 'none';
			}
		} else {
				el.parentNode.style.display = 'flex';
		}
	});
}
  
  
async _onClickZeroSkillToggle(event, actor) {
    event.preventDefault();	
	this._show_zero_skills = !this._show_zero_skills;
	this._updateSkillVisibility();
}
	

async _onClickSkill(event, actor) {
    event.preventDefault();
    const a = event.currentTarget;
	const skillname = a.getAttribute("data-skill");
	
	this.actorskillroll(actor, skillname, 0, "");
  }

async _onClickItem(event, actor) {
    event.preventDefault();
    let li = $(event.currentTarget).parents(".item"),
        item = this.actor.getOwnedItem(li.data("item-id"));
    
    // Do we have a skill to roll on?
    if (item.data.data.attributes.skill) {
        let skillname = item.data.data.attributes.skill.value
 		let modifier = 0;
		if (item.data.data.attributes.skillmod) {        
            modifier = item.data.data.attributes.skillmod.value;
        }
		
		this.actorskillroll(actor, skillname, modifier, "Using "+item.name+". ");
		
    } else if (item.type == "weapon") {
		console.log("Weapon actions for " + item.name);
		let d = new Dialog({
            title: actor.name+"'s weapon actions for "+ item.name,
            content: '<p>Select weapon action:</p>',
            buttons: {
                one: {
                    icon: '<i class="fas fa-dot-circle"></i>',
                    label: "Single Shot",
                    callback: () => {
                        // Determine the skill to use
                        let skill_per_wt = {
                            P:"handgun",
                            RIF:"rifle",
                            SMG:"submachinegun",
                            SHT:"rifle",
                            HVY:"heavyweapons",
                            MEL:"melee",
                            THROW:"athletics"
                        };
                        
                        this.actorskillroll(actor, skill_per_wt[item.data.data.attributes.weapontype.value], 
                            item.data.data.attributes.wa.value, "Attacking with "+item.name+". ");
                    }
                },
                two: {
                    icon: '<i class="fas fa-bomb"></i>',
                    label: "Damage & Location",
                    callback: () => {
                    
                        let damage_formula = item.data.data.attributes.damage.value;
						ChatMessage.create({
                            user: game.user._id,
                            content: "Rolling damage from "+ item.name +": [[" + damage_formula + "]] damage on hit location [[ 1d10 ]].",
                            }, {});
                    }
                }
            },
            default: "one",
            close: () => console.log("Weapon actions closed.")
		});
		d.render(true);
	}
  }

actorskillroll(actor, skillname, modifier, flavourtext) {
        let actorskill = actor.data.data.skills[skillname];
        let statname = actorskill.stat;
        if (statname == 'special') return;
        
        console.log(actor.data.data.item_modifiers);
        console.log(statname+"_mod");
        
        let formula = "1d10x10 + " + actor.data.data.stats[statname].value  + " + " + actor.data.data.item_modifiers[statname+"_mod"].value  + " + " + actorskill.value; 
        if (skillname == 'awarenessnotice') {formula += " + " + actor.data.data.skills["combatsense"].value} 
        
        // Check if the item has a skill modifier
        if (modifier) {        
            formula += " + " + modifier;
        }

        // Do the roll thing
        ChatMessage.create({
            user: game.user._id,
            content: flavourtext +" Rolling "+ actorskill.label+" for "+ actor.name+" [[" + formula + "]]",
        }, {});
}

  async _onItemAction(event, actor) {
    event.preventDefault();
    const a = event.currentTarget;
	const action_name = a.getAttribute("data-action");
	console.log("Item action "+action_name);
  }

  /* -------------------------------------------- */

  async _onClickAttributeControl(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;
    const attrs = this.object.data.data.attributes;
    const form = this.form;
	
	console.log("_onClickAttributeControl");

    // Add new attribute
    if ( action === "create" ) {
      const nk = Object.keys(attrs).length + 1;
      let newKey = document.createElement("div");
      newKey.innerHTML = `<input type="text" name="data.attributes.attr${nk}.key" value="attr${nk}"/>`;
      newKey = newKey.children[0];
      form.appendChild(newKey);
      await this._onSubmit(event);
    }

    // Remove existing attribute
    else if ( action === "delete" ) {
      const li = a.closest(".attribute");
      li.parentElement.removeChild(li);
      await this._onSubmit(event);
    }
  }

  /* -------------------------------------------- */

  /**
   * Implement the _updateObject method as required by the parent class spec
   * This defines how to update the subject of the form when the form is submitted
   * @private
   */
  _updateObject(event, formData) {

    // Handle the free-form attributes list
    const formAttrs = expandObject(formData).data.attributes || {};
    const attributes = Object.values(formAttrs).reduce((obj, v) => {
      let k = v["key"].trim();
      if ( /[\s\.]/.test(k) )  return ui.notifications.error("Attribute keys may not contain spaces or periods");
      delete v["key"];
      obj[k] = v;
      return obj;
    }, {});
    
    // Remove attributes which are no longer used
    for ( let k of Object.keys(this.object.data.data.attributes) ) {
      if ( !attributes.hasOwnProperty(k) ) attributes[`-=${k}`] = null;
    }

    // Re-combine formData
    formData = Object.entries(formData).filter(e => !e[0].startsWith("data.attributes")).reduce((obj, e) => {
      obj[e[0]] = e[1];
      return obj;
    }, {_id: this.object._id, "data.attributes": attributes});

      
    // Update the Actor
    return this.object.update(formData);
  }
}
