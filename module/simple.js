/**
 * A simple and flexible system for world-building using an arbitrary collection of character and item attributes
 * Author: Atropos
 * Software License: GNU GPLv3
 */

// Import Modules
import { SimpleItemSheet } from "./item-sheet.js";
import { SimpleActorSheet } from "./actor-sheet.js";
import { CP2020ActorSheet } from "./cp2020-actor-sheet.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function() {
  console.log(`Initializing Simple Worldbuilding System`);

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  //Actors.registerSheet("simple", SimpleActorSheet, { makeDefault: true });
  Actors.registerSheet("cp2020", CP2020ActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("simple", SimpleItemSheet, {makeDefault: true});
});



// Handle item modifiers when actors gain/lose items.
// We don't trust the events to fire consistently, so 
// recalculate everything whenever something changes.

function updateItemModifiers(actor) {
    console.log(actor);
    // Set all current modifiers to 0
    for (var key in actor.data.data.item_modifiers) {
        actor.data.data.item_modifiers[key].value = 0;
    }
    for (var key in actor.data.data.armor) {
        actor.data.data.armor[key] = 0;
    }

    // Accumulate the current modifiers
    for (let item of actor.items) {
        // Iterate through the item attribs looking for modifiers:
        for (var key in item.data.data.attributes) {
            if (key.endsWith('_mod')) {
                actor.data.data.item_modifiers[key].value += parseFloat(item.data.data.attributes[key].value);
            }
            if (key.startsWith('armor_')) {
                actor.data.data.armor[key] += parseFloat(item.data.data.attributes[key].value);
            }
        }
    }
    
    actor.update({"data.item_modifiers":actor.data.data.item_modifiers, "data.armor":actor.data.data.armor});
    console.log(actor);
}


Hooks.on('createOwnedItem', (actor, item, delta) => {
    updateItemModifiers(actor);
});

Hooks.on('updateOwnedItem', (actor, item, delta) => {
    updateItemModifiers(actor);
});

Hooks.on('deleteOwnedItem', (actor, item, delta) => {
    updateItemModifiers(actor);
});